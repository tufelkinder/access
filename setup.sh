systemctl enable ssh
systemctl start ssh

echo "alias dir='ls -al'" >> /root/.bashrc

apt-get update;apt -y upgrade;apt-get -y autoremove

dpkg-reconfigure tzdata

apt-get install -y git postgresql nginx uwsgi uwsgi-plugin-python3 python-pip python3-pip libzbar-dev libzbar0 python-dev python3-dev build-essential libffi-dev libssl-dev libpq-dev locate dialog

cd /usr/src

git clone https://github.com/yoyojacky/52Pi.git
cd 52Pi
chmod +x restool.sh
./restool.sh

sudo -u postgres createuser pippin -s -P
# enter password: 
sudo -u postgres createdb access -O pippin

echo "local   all             pippin                                  md5" >> /etc/postgresql/11/main/pg_hba.conf

service postgresql restart

cd /var/www

git clone https://tufelkinder@bitbucket.org/tufelkinder/access.git

ln -s /var/www/access/py /var/www/py

mkdir -p /var/www/web/access/static
ln -s /usr/local/lib/python3.7/dist-packages/django/contrib/admin/static/admin /var/www/web/access/static
cd /var/www/access

cp access.conf /etc/nginx/sites-available
ln -s /etc/nginx/sites-available/access.conf /etc/nginx/sites-enabled/access.conf

rm /etc/nginx/sites-enabled/default

cp access.ini /etc/uwsgi/apps-available
ln -s /etc/uwsgi/apps-available/access.ini /etc/uwsgi/apps-enabled/

cp access-controller.service /etc/systemd/system

systemctl enable access-controller.service

#pip install -r requirements.txt
pip3 install -r requirements.txt

cd py/access
chmod +x manage.py

# create settings file!
pico access/settings.py

./manage.py makemigrations
./manage.py migrate
./manage.py createsuperuser  # dennis

service uwsgi restart
service nginx restart

