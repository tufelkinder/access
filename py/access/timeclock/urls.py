"""access URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.urls import path
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.contrib.auth import views as auth_views
from timeclock import views


urlpatterns = [
    url(r'^$', login_required(views.index)),
    url(r'^reports/$', login_required(views.reports)),
    url(r'^sms/receive/$', views.sms_receive),
    path('clockevent/add/', views.ClockEventCreate.as_view(), name='clockevent-add'),
    path('clockevent/update/<int:pk>/', views.ClockEventUpdate.as_view(), name='clockevent-update'),
    path('clockevent/detail/<int:pk>/', views.ClockEventDetail.as_view(), name='clockevent-detail'),
    # path('segment/update/<int:pk>/', views.SegmentUpdate.as_view(), name='segment-update'),
    path('segment/update/<int:pk>/', views.segment_update, name='segment-update'),
    path('segment/detail/<int:pk>/', views.SegmentDetail.as_view(), name='segment-detail'),
    path('segment/delete/<int:pk>/', views.segment_delete, name='segment-delete'),
    path('shift/create/', views.ShiftCreate.as_view(), name='shift-create'),
    path('shift/update/<int:pk>/', views.ShiftUpdate.as_view(), name='shift-update'),
    path('shift/detail/<int:pk>/', views.ShiftDetail.as_view(), name='shift-detail'),
    path('accounts/login/', auth_views.LoginView.as_view(template_name='timeclock/login.html'), name='login'),
    path('accounts/login/', auth_views.LoginView.as_view(template_name='timeclock/login.html'), name='password_reset'),
    path('export/<slug:s_date>/<slug:e_date>/', views.export_isolved, name='export-isolved'),
    path('sclock/', views.sclock, name="sclock"),
    path('ssn_clock/', views.ssn_clock, name="ssn_clock"),
    path('get_in_emps/', views.get_clockedin_emps, name='get_in_emps'),
    url(r'^admin/', admin.site.urls),
]
