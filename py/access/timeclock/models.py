import json
import time
from pytz import timezone
from django.utils import timezone as dj_tz
from decimal import Decimal
from datetime import date, datetime, timedelta
from django.utils.translation import ugettext as _
from django.db import models
from django.contrib.auth.models import User, Group
from django.db.models import Q  #, F, Max, Min, Count, Sum, Avg
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.urls import reverse
from cryptography.hazmat.primitives import serialization as crypto_serialization, hashes
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend as crypto_default_backend
from cryptography.hazmat.primitives.serialization import load_pem_private_key, load_pem_public_key
from twilio.rest import Client
from server import actions as a
from server.models import BaseModel, TimeClock, Person, Log

EST = timezone('US/Eastern')
# Create your models here.

account_sid = 'AC78206534f90af3a363f9e6bbaa522a3e'
auth_token = '89472df5af87ff4473d62b7f667bf919'
ME = '+17173569858'


class ClockEvent(BaseModel):
    time_clock = models.ForeignKey(TimeClock, on_delete=models.CASCADE)
    employee = models.ForeignKey(Person, on_delete=models.CASCADE)
    date_time = models.DateTimeField(null=True, blank=True)
    adjustment = models.BooleanField(default=False)

    class Meta:
        ordering = ('-date_created',)

    def __str__(self):
        return '{0} via {1} at {2}'.format(self.employee, self.time_clock, self.date_created)

    def serialize(self):
        return { 
            'time_clock': self.time_clock.id,
            'employee': self.employee.id,
            'date_time': self.date_time
        }


class Shift(BaseModel):
    employee = models.ForeignKey(Person, on_delete=models.CASCADE)
    date = models.DateField(null=True, blank=True)

    def __str__(self):
        return '{0} - {1}'.format(self.employee, self.date)

    def total_time_data(self):
        x = sum([d.duration() for d in self.segment_set.all()], timedelta(0,0))
        return x

    def total_time(self):
        return time.strftime('%H:%M:%S', time.gmtime(self.total_time_data().seconds))

    def serialize(self):
        segs = {}
        for s in self.segment_set.all():
            ti = dj_tz.localtime(s.time_in).strftime('%I:%M %p')
            to = dj_tz.localtime(s.time_out).strftime('%I:%M %p') if s.time_out else ''
            segs[s.id] = '{0} - {1}'.format(ti, to)
        return {
            'shift_id': self.id,
            'employee_id': self.employee.id,
            'employee_name': self.employee.first_name + ' ' + self.employee.last_name,
            'segments': segs
        }

    def get_absolute_url(self):
        return reverse('shift-detail', kwargs={'pk': self.pk})


class Segment(BaseModel):
    shift = models.ForeignKey(Shift, on_delete=models.CASCADE)
    clock_in = models.ForeignKey(ClockEvent, null=True, on_delete=models.CASCADE, related_name='segment_in')
    time_in = models.DateTimeField(null=True, blank=True)
    clock_out = models.ForeignKey(ClockEvent, on_delete=models.SET_NULL, null=True, blank=True, related_name='segment_out')
    time_out = models.DateTimeField(null=True, blank=True)
    adjusted = models.BooleanField(default=False, editable=False)
    adjusted_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, editable=False)

    def __str__(self):
        return '{0} -- In: {1}, Out: {2}'.format(self.clock_in.employee, self.time_in, self.time_out)

    def save(self, *args, **kwargs):
        if self.clock_in and not self.time_in:
            self.time_in = self.clock_in.date_time
        if self.clock_out and not self.time_out:
            self.time_out = self.clock_out.date_time 
        super(Segment, self).save(*args, **kwargs)
       
    def duration(self):
        now = dj_tz.localtime()
        try:
            if self.time_in and self.time_out:
                end_time = self.time_out
            elif self.time_in.date() == now.date():
                end_time = now
            else:
                d = self.time_in.date()
                end_time = datetime(d.year, d.month, d.day, 23, 59, 59, 0, EST)
            return end_time - self.time_in
        except Exception as e:
            log = Log.objects.create(message=str(e))
        return timedelta(days=1)

    def serialize(self):
        return {
            'segment_id': self.id,
            'employee_id': self.shift.employee.id,
            'employee_name': self.shift.employee.first_name + ' ' + self.shift.employee.last_name,
            'clock_in_id': self.clock_in.id,
            'time_in': dj_tz.localtime(self.time_in).strftime('%Y-%m-%dT%H:%M'),
            'clock_out_id': self.clock_out.id if self.clock_out else '',
            'time_out': dj_tz.localtime(self.time_out).strftime('%Y-%m-%dT%H:%M') if self.time_out else ''
        }


class AdminLeave(BaseModel):
    description = models.TextField(null=True, blank=True)
    date = models.DateField()
    include_active = models.BooleanField(default=False, help_text='Add all clocked in employees.')
    employees = models.ManyToManyField(Person, blank=True, limit_choices_to={'active': True})
    hours = models.DecimalField(max_digits=5, decimal_places=2, help_text='Hours to fill to.')

    def __str__(self):
        return str(self.date)

    def employee_count(self):
        return self.employees.all().count()


class SMSMessage(BaseModel):
    sender = models.CharField(max_length=100)
    recipient = models.CharField(max_length=100, null=True, blank=True)
    body = models.TextField(null=True, blank=True)
    sid = models.CharField(max_length=255, null=True, blank=True)
    date_created = models.DateTimeField(auto_now_add=True)


def lt(t):
    """ Default time zone application and string formatting of time object t """
    return dj_tz.localtime(t).strftime('%I:%M %p')


def sms_send(sms):
    client = Client(account_sid, auth_token)
    message = client.messages.create(
        body=sms.body,
        from_=sms.sender,
        to=sms.recipient
    )
    sms.id = message.sid
    return sms


@receiver(post_save, sender=ClockEvent)
def link_ce_to_segment(sender, instance, created, *args, **kwargs):
    if created and not instance.adjustment:  # not for manual adjustments
        active_segments = Segment.objects.filter(clock_out__isnull=True, clock_in__employee=instance.employee, date_created__date=dj_tz.localdate())
        if active_segments.exists():
            active_segment = active_segments[0]
            active_segment.clock_out = instance
            active_segment.save()
            msg = 'You have clocked OUT at {0}.'.format(lt(active_segment.time_out))
        else:
            shift, created = Shift.objects.get_or_create(employee=instance.employee, date=dj_tz.localdate())
            segment = Segment(
                shift=shift,
                clock_in=instance
            )
            segment.save()
            msg = 'You have clocked IN at {0}.'.format(lt(segment.time_in))
        sms = SMSMessage.objects.create(
            sender=ME,
            recipient=instance.employee.phone,
            body=msg
        )
        sms_send(sms)
    else:
        # update any linked segments
        segs = Segment.objects.filter(clock_in=instance)
        if segs.exists():
            segs[0].time_in = instance.date_time
            segs[0].save()
        else:
            segs = Segment.objects.filter(clock_out=instance)
            if segs.exists():
                segs[0].time_out = instance.date_time
                segs[0].save()


