from django.contrib import admin
from timeclock.models import *

# Register your models here.

class SegmentInline(admin.TabularInline):
    model = Segment
    extra = 0
    # raw_id_fields = ('clock_in', 'clock_out')


class ClockEventAdmin(admin.ModelAdmin):
    list_display = ('employee', 'time_clock', 'date_time', 'date_created', 'adjustment')
    list_filter = ('employee', 'time_clock')


class ShiftAdmin(admin.ModelAdmin):
    list_display = ('employee', 'date', 'total_time')
    list_filter = ('employee', )
    # inlines = [SegmentInline]
    date_hierarchy = 'date'


class SegmentAdmin(admin.ModelAdmin):
    list_display = ('clock_in', 'time_in', 'clock_out', 'time_out', 'duration')
    list_filter = ('clock_in__employee',)
    raw_id_fields = ('shift', 'clock_in', 'clock_out')
    date_hierarchy = 'time_in'


class AdminLeaveAdmin(admin.ModelAdmin):
    list_display = ('date', 'description', 'include_active', 'hours', 'employee_count')
    filter_horizontal = ('employees',)

    class Media:
        js = ('/static/js/jquery-3.3.1.min.js', '/static/js/admin_timeclock.js',)


class SMSMessageAdmin(admin.ModelAdmin):
    list_display = ('sender', 'recipient', 'date_created', 'sid')
    search_fields = ('sender', 'recipient', 'body', 'sid')


admin.site.register(ClockEvent, ClockEventAdmin)
admin.site.register(Shift, ShiftAdmin)
admin.site.register(Segment, SegmentAdmin)
admin.site.register(SMSMessage, SMSMessageAdmin)
admin.site.register(AdminLeave, AdminLeaveAdmin)
