from datetime import timedelta
from django.utils import timezone as dj_tz
from server.models import Person
from timeclock.models import Shift

pavel = Person.objects.get(pk=37)
danielc = Person.objects.get(pk=32)

pshifts = Shift.objects.filter(employee=pavel, date__gte=dj_tz.localdate()-timedelta(days=30))
dshifts = Shift.objects.filter(employee=danielc, date__gte=dj_tz.localdate()-timedelta(days=30))


def ftime(t):
	return dj_tz.localtime(t).strftime('%m/%d/%Y - %I:%M %p')


for s in pshifts:
	beg = s.segment_set.all().first().time_in
	end = s.segment_set.all().last().time_out
	print('IN:', ftime(beg), '; OUT:', ftime(end), ' = ', s.total_time())


for s in dshifts:
	beg = s.segment_set.all().first().time_in
	end = s.segment_set.all().last().time_out
	print('IN:', ftime(beg), '; OUT:', ftime(end), ' = ', s.total_time())

