import pytz
import json
from decimal import Decimal
from django.utils.dateparse import parse_datetime
from datetime import date, datetime, timedelta, time
from django.utils import timezone as dj_tz
from django.http import HttpResponse, JsonResponse, FileResponse, HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.detail import BaseDetailView
from openpyxl import Workbook
from twilio.rest import Client
from twilio.twiml.messaging_response import MessagingResponse
from server.models import Person, TimeClock, Holiday, HolidayDate, Log
from .models import ClockEvent, Shift, Segment, SMSMessage, AdminLeave, EST, account_sid, auth_token, ME

DAYS = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
FORTY = timedelta(seconds=144000)
ZERO = timedelta(seconds=0)


# Create your views here.


def dec_time(d):
	return round(d.total_seconds()/3600, 2)


class JSONResponseMixin:
	"""
	A mixin that can be used to render a JSON response.
	"""
	def render_to_json_response(self, context, **response_kwargs):
		"""
		Returns a JSON response, transforming 'context' to make the payload.
		"""
		return JsonResponse(self.get_data(context), **response_kwargs)

	def get_data(self, context):
		"""
		Returns an object that will be serialized as JSON by json.dumps().
		"""
		return self.object.serialize()


def index(request):
	if request.POST:
		d = date(*[int(x) for x in request.POST.get('date').split('-')])
		dept = request.POST.get('dept')
	else:
		d = dj_tz.localdate()
	shifts = Shift.objects.filter(date=d)
	return render(request, 'timeclock/index.html', {
		'cur_date': d,
		'shifts': shifts,
		'employees': Person.objects.filter(active=True),
		'time_clocks': TimeClock.objects.all(),
		'departments': []
	})


def reports(request):
	end = dj_tz.localdate()
	start = None
	if request.POST:
		param_start = request.POST.get('start')
		param_end = request.POST.get('end')
		if param_end:
			end = date(*[int(x) for x in param_end.split('-')])
			if param_start:
				start = date(*[int(x) for x in param_start.split('-')])
		else:
			if param_start:
				start = date(*[int(x) for x in param_start.split('-')])
				end = start + timedelta(days=13)
		dept = request.POST.get('dept')
	if not start:
		start = end - timedelta(days=13)
	shifts = Shift.objects.filter(date__gte=start, date__lte=end).order_by('employee__last_name', 'employee__first_name', 'date')
	employees = shifts.values('employee').distinct().order_by('employee__last_name', 'employee__first_name')
	return render(request, 'timeclock/reports.html', {
		'start': start,
		'end': end,
		'shifts': shifts,
		'employees': employees,
		'time_clocks': TimeClock.objects.all(),
		'departments': []
	})


def calc_leave(employee, start_date, end_date):
	leaves = AdminLeave.objects.filter(employees=employee, date__range=(start_date, end_date))
	leave_time = Decimal(0)
	if leaves:
		for l in leaves:
			# try:
			shift = Shift.objects.get(employee=employee, date=l.date)
			worked_hours = dec_time(shift.total_time_data())
			leave_hours = l.hours - Decimal(worked_hours) if worked_hours < l.hours else ZERO
			leave_time += leave_hours
			# except:
			#  	pass
	return leave_time


def export_isolved(request, s_date, e_date):
	s_date = datetime.strptime(s_date, '%Y-%m-%d')
	e_date = datetime.strptime(e_date, '%Y-%m-%d')
	fn = '/tmp/isolved_export.xlsx'
	header = [
		'Legal', 'PayGroup', 'Division', 'Location', 'Key', 'Name',
		'LaborValue2', 'LaborValue3', 'LaborValue4', 'E_Regular_Hours',
		'E_Overtime_Hours', 'E_Holiday_Hours', 'E_Jury Duty_Hours',
		'E_Funeral_Hours', 'E_Mileage_Dollars', 'E_Bonus_Dollars', 'E_Admin Leave_Hours'
	]
	pre_fields = ['CN53135', 'Bi-Weekly']
	# calc mid-pay-period start week, two shift queries, two emp queries, etc,. that are then combined.
	mid_monday = s_date + timedelta(days=7)
	shifts1 = Shift.objects.filter(date__gte=s_date, date__lt=mid_monday).order_by('date')
	shifts2 = Shift.objects.filter(date__gte=mid_monday, date__lte=e_date).order_by('date')
	employees = Person.objects.filter(active=True).order_by('last_name', 'first_name')
	holiday1_count = HolidayDate.objects.filter(date_observed__gte=s_date, date_observed__lt=mid_monday).count()  # add recurs_on_date Q() query
	holiday2_count = HolidayDate.objects.filter(date_observed__gte=mid_monday, date_observed__lte=e_date).count()  # add recurs_on_date Q() query
	holiday1_hours = holiday1_count * timedelta(seconds=28800)
	holiday2_hours = holiday2_count * timedelta(seconds=28800)
	hour_data = {}
	rows = []
	for e in employees:
		s1_total_hours = sum([s.total_time_data() for s in shifts1.filter(employee=e)], timedelta(0,0))
		s2_total_hours = sum([s.total_time_data() for s in shifts2.filter(employee=e)], timedelta(0,0))
		hol1_hours, hol2_hours = ZERO, ZERO
		if s1_total_hours >= FORTY:
			s1_work_hours = FORTY
			s1_over_hours = s1_total_hours - FORTY
		else:
			s1_work_hours = s1_total_hours
			s1_over_hours = timedelta(seconds=0)
		if s2_total_hours >= FORTY:
			s2_work_hours = FORTY
			s2_over_hours = s2_total_hours - FORTY
		else:
			s2_work_hours = s2_total_hours
			s2_over_hours = timedelta(seconds=0)
		if e.emp_class == 'F':
			hol1_hours = s1_total_hours + holiday1_hours
			hol2_hours = s2_total_hours + holiday2_hours
			if s1_over_hours:
				s1_over_hours += holiday1_hours
				s1_work_hours -= holiday1_hours
			elif hol1_hours > FORTY:
				diff_hrs = hol1_hours - FORTY
				s1_over_hours += diff_hrs
				s1_work_hours -= diff_hrs
			if s2_over_hours:
				s2_over_hours += holiday2_hours
				s2_work_hours -= holiday2_hours
			elif hol2_hours > FORTY:
				diff_hrs = hol2_hours - FORTY
				s2_over_hours += diff_hrs
				s2_work_hours -= diff_hrs
		adm_leave = calc_leave(e, s_date, e_date)
		hour_data[e.id] = [
			e.division.name, e.division.short_loc(), e.employee_id,
			str(e), '', '', '', 
			dec_time(s1_work_hours + s2_work_hours),
			dec_time(s1_over_hours + s2_over_hours),
			dec_time(holiday1_hours + holiday2_hours) if e.emp_class == 'F' else 0.0,
			'', '', '', '',
			adm_leave
		]
	wb = Workbook()
	ws = wb.active
	ws.append(header)
	for key in hour_data.keys():
		if hour_data[key][7] > 0 or hour_data[key][9] > 0:
			ws.append(pre_fields + hour_data[key])
	wb.save(fn)
	return FileResponse(open(fn, 'rb'))


def sms_send(sms):
	client = Client(account_sid, auth_token)
	message = client.messages.create(
		body=sms.body,
		from_=sms.sender,
		to=sms.recipient
	)
	sms.id = message.sid
	return sms


@csrf_exempt
def sms_receive(request):
	p = request.POST.dict()
	try:
		sender = p.get('From')
		recipient = p.get('To')
		body = p.get('Body')
		sid = p.get('SmsSid')
		sms = SMSMessage.objects.create(
			sender=sender,
			recipient=recipient,
			body=body,
			sid=sid
		)
		person = Person.objects.get(phone=sender)
		if person:
			active_segments = Segment.objects.filter(clock_out__isnull=True, clock_in__employee=person, date_created__date=dj_tz.localdate())
			if 'in' in body.lower() or 'out' in body.lower():
				if person.mobile_clock:
					if active_segments.exists():
						action = 'OUT'
					else:
						action = 'IN'
					clock = TimeClock.objects.get(id=5)
					ce = ClockEvent(time_clock=clock, date_time=dj_tz.localtime(), employee=person)
					ce.save()
					sms = SMSMessage.objects.create(
						sender=ME,
						recipient=sender,
						body='Thank you, {0}, you are now clocked {1}.'.format(person.first_name, action),
						sid=sid
					)
				else:
					sms = SMSMessage.objects.create(
						sender=ME,
						recipient=sender,
						body="Hi {0}, You're not authorized for mobile clock-in.".format(person.first_name),
						sid=sid
					)
				sms_send(sms)
			elif person and '?' in body:
				today = dj_tz.localdate()
				if today.weekday() != 0:
					msg = 'Hi {0},\n'.format(person.first_name)
					last_monday = today - timedelta(days=today.weekday())
					week_hours = 0
					for x in range(last_monday.weekday(), today.weekday()):
						d = last_monday + timedelta(days=x)
						d_shifts = Shift.objects.filter(employee=person, date=d)
						day_hours = d_shifts[0].total_time() if d_shifts else '0'
						msg += 'On {0} you worked {1} hours\n'.format(DAYS[x], day_hours)
						week_hours += day_hours
					today_segs = Segment.objects.filter(clock_in__employee=person, date_created__date=today)
					today_shifts = Shift.objects.filter(employee=person, date=today)
					# if active_segments.exists():
					#     msg += "\nYou last clocked in at {1} today.".format(person.first_name, lt(active_segments[0].time_in))
					if today_segs.exists():
						msg += 'You'  # .format(person.first_name)
						for s in today_segs:
							base = " clocked in at {0} and out at {1}"
							if len(today_segs) > 1:
								base += ','
							msg += base.format(lt(s.time_in), lt(s.time_out))
						msg += ' and worked a total of {0} hours so far.'.format(today_shifts[0].total_time())
						week_hours += today_shifts[0].total_time()
					else:
						msg += " You don't appear to have clocked in today."
				else:
					today_segs = Segment.objects.filter(clock_in__employee=person, date_created__date=today)
					today_shifts = Shift.objects.filter(employee=person, date=today)
					# if active_segments.exists():
					#     msg = "Hi {0}, You last clocked in at {1} today.".format(person.first_name, lt(active_segments[0].time_in))
					if today_segs.exists():
						msg = 'Hi {0}, You'.format(person.first_name)
						for s in today_segs:
							base = " clocked in at {0} and out at {1}"
							if len(today_segs) > 1:
								base += ','
							msg += base.format(lt(s.time_in), lt(s.time_out))
						msg += ' and worked a total of {0} hours so far.'.format(today_shifts[0].total_time())
					else:
						msg = "Hi {0}, You don't appear to have clocked in today.".format(person.first_name)
				sms = SMSMessage.objects.create(
					sender=ME,
					recipient=sender,
					body=msg,
					sid=sid
				)
				sms_send(sms)
			else:
				sms = SMSMessage.objects.create(
					sender=ME,
					recipient=sender,
					body="Hey... what's up? Who is this? -- 187",
					sid=sid
				)
				sms_send(sms)
		else:
			sms = SMSMessage.objects.create(
				sender=ME,
				recipient=sender,
				body="Hey... what's up? how are you? -- 195",
				sid=sid
			)
			sms_send(sms)
	except Exception as e:
		log = Log.objects.create(message=str(e) + '\n' + str(p) + '\nSender: ' + sender + '\nMe: ' + ME)
		sms = SMSMessage.objects.create(
			sender=ME,
			recipient=sender,
			body='Unknown command or number. Please check with your administrator.',
			sid=sid
		)
	return HttpResponse('')


def segment_update(request, pk=None):
	# segment = Segment.objects.get(pk=pk)
	# if request.POST:
	seg_id = request.POST.get('segment_id', pk)
	emp_id = request.POST.get('employee_id')
	clock_in_id = request.POST.get('clock_in_id')
	clock_out_id = request.POST.get('clock_out_id')
	time_in = request.POST.get('time_in')
	time_out = request.POST.get('time_out')

	segment = Segment.objects.get(pk=seg_id)
	employee = Person.objects.get(pk=emp_id)

	if time_in:
		time_in = EST.localize(parse_datetime(time_in))
		if clock_in_id:
			# update existing ClockEvent
			ce = ClockEvent.objects.get(pk=clock_in_id)
			ce.date_time = time_in
			ce.adjustment = True
			ce.modified_by = request.user
			ce.save()
			segment.time_in = time_in
			segment.adjusted = True
			segment.adjusted_by = request.user
			segment.save()
	if time_out:
		time_out = EST.localize(parse_datetime(time_out))
		if clock_out_id:
			ce = ClockEvent.objects.get(pk=clock_out_id)
			ce.date_time = time_out
			ce.adjustment = True
			ce.modified_by = request.user
			ce.save()
			segment.time_out = time_out
			segment.adjusted = True
			segment.adjusted_by = request.user
			segment.save()
		else:
			ce = ClockEvent.objects.create(
				time_clock=TimeClock.objects.get(pk=1),
				employee=employee,
				date_time=time_out,
				adjustment=True,
				modified_by=request.user
			)
			segment.clock_out = ce
			segment.adjusted = True
			segment.adjusted_by = request.user
			segment.save()
		# create new CE and save as segment time_out
	return JsonResponse(segment.shift.serialize())


def segment_delete(request, pk=None):
	segment = Segment.objects.get(pk=pk)
	segment.clock_out.delete()
	segment.delete()
	return JsonResponse({'status': 'success', 'id': pk})


class ClockEventCreate(CreateView):
	model = ClockEvent
	fields = ['time_clock', 'employee', 'date_time']

	def get_initial(self):
		initial = super(ClockEventCreate, self).get_initial()
		if self.request.user.is_authenticated:
			initial['time_clock'] = 10  # manual entry time clock??
			initial['employee'] = self.request.GET.get('employee_id')  # set the employee for this line
		return initial

	def form_valid(self, form):
		form.instance.created_by = self.request.user
		return super().form_valid(form)


class ClockEventUpdate(UpdateView):
	model = ClockEvent
	fields = ['time_clock', 'employee', 'date_time']

	def form_valid(self, form):
		form.instance.modified_by = self.request.user
		return super().form_valid(form)


class ClockEventDetail(JSONResponseMixin, BaseDetailView):
	model = ClockEvent

	def render_to_response(self, context, **response_kwargs):
		return self.render_to_json_response(context, **response_kwargs)


class SegmentUpdate(UpdateView):
	model = Segment

	def form_valid(self, form):
		form.instance.modified_by = self.request.user
		return super().form_valid(form)


class SegmentDetail(JSONResponseMixin, BaseDetailView):
	model = Segment

	def render_to_response(self, context, **response_kwargs):
		return self.render_to_json_response(context, **response_kwargs)


class SegmentDelete(JSONResponseMixin, DeleteView):
	model = Segment

	def render_to_response(self, context, **response_kwargs):
		return self.render_to_json_response(context, **response_kwargs)


class ShiftCreate(CreateView):
	model = Shift
	fields = ['employee', 'date']

	def form_valid(self, form):
		form.instance.created_by = self.request.user
		response = super().form_valid(form)
		# create two basic segments with two clockevents each?
		ce_in = ClockEvent.objects.create(
			time_clock=TimeClock.objects.get(pk=10),
			employee=self.object.employee,
			date_time=self.object.date,
			adjustment=True
		)
		ce_out = ClockEvent.objects.create(
			time_clock=TimeClock.objects.get(pk=10),
			employee=self.object.employee,
			date_time=self.object.date,
			adjustment=True
		)
		seg = Segment(
			shift=self.object,
			clock_in=ce_in,
			clock_out=ce_out,
			adjusted=True,
			adjusted_by=self.request.user
		)
		seg.save()
		ce_in = ClockEvent.objects.create(
			time_clock=TimeClock.objects.get(pk=10),
			employee=self.object.employee,
			date_time=self.object.date,
			adjustment=True
		)
		ce_out = ClockEvent.objects.create(
			time_clock=TimeClock.objects.get(pk=10),
			employee=self.object.employee,
			date_time=self.object.date,
			adjustment=True
		)
		seg = Segment(
			shift=self.object,
			clock_in=ce_in,
			clock_out=ce_out,
			adjusted=True,
			adjusted_by=self.request.user
		)
		seg.save()
		ce_in = ClockEvent.objects.create(
			time_clock=TimeClock.objects.get(pk=10),
			employee=self.object.employee,
			date_time=self.object.date,
			adjustment=True
		)
		ce_out = ClockEvent.objects.create(
			time_clock=TimeClock.objects.get(pk=10),
			employee=self.object.employee,
			date_time=self.object.date,
			adjustment=True
		)
		seg = Segment(
			shift=self.object,
			clock_in=ce_in,
			clock_out=ce_out,
			adjusted=True,
			adjusted_by=self.request.user
		)
		seg.save()
		return response

	def render_to_response(self, context, **response_kwargs):
		return self.render_to_json_response(context, **response_kwargs)


class ShiftUpdate(UpdateView):
	model = Shift

	def form_valid(self, form):
		form.instance.modified_by = self.request.user
		return super().form_valid(form)


class ShiftDetail(JSONResponseMixin, BaseDetailView):
	model = Shift

	def render_to_response(self, context, **response_kwargs):
		return self.render_to_json_response(context, **response_kwargs)


def lt(t):
	return dj_tz.localtime(t).strftime('%I:%M %p')


def sclock(request):
	try:
		if request.POST:
			emp = Person.objects.get(ssn=request.POST.get('user_id'))
		elif request.GET:
			emp = Person.objects.get(ssn=request.GET.get('user_id'))
	except:
		return HttpResponseRedirect('/accounts/login/')
	active_segments = Segment.objects.filter(
		clock_out__isnull=True,
		clock_in__employee=emp,
		date_created__date=dj_tz.localdate()
	)
	segments = Segment.objects.filter(
		clock_in__employee=emp,
		clock_out__isnull=False,
		date_created__date=dj_tz.localdate()
	)
	return render(request, 'timeclock/sclock.html', {
		'employee': emp,
		'active_segments': active_segments,
		'segments': segments,
	})


def ssn_clock(request):
	if request.POST:
		# try:
		emp = Person.objects.get(pk=request.POST.get('employee_id'))
		# except:
		#     return HttpResponseRedirect('/accounts/login/')
		direction = request.POST.get('direction')
		message = ''
		if direction == 'out':
			active_segments = Segment.objects.filter(
				clock_out__isnull=True,
				clock_in__employee=emp,
				date_created__date=dj_tz.localtime()
			)
			if not active_segments:
				message = 'You are not clocked in.'
			else:
				ce = ClockEvent(
					time_clock=TimeClock.objects.get(pk=9),
					employee=emp,
					date_time=dj_tz.localtime()
				)
				ce.save()
				message = 'You have clocked out.'
				return HttpResponseRedirect('/sclock/?user_id={}'.format(emp.ssn))
		elif direction == 'in':
			ce = ClockEvent(
				time_clock=TimeClock.objects.get(pk=9),
				employee=emp,
				date_time=dj_tz.localtime()
			)
			ce.save()
			message = 'You have clocked in.'
			return HttpResponseRedirect('/sclock/?user_id={}'.format(emp.ssn))
		segments = Segment.objects.filter(
			clock_in__employee=emp,
			clock_out__isnull=False,
			date_created__date=dj_tz.localdate()
		)
	return render(request, 'timeclock/sclock.html', {
		'message': message,
		'employee': emp,
		'active_segments': segments,
		'segments': segments,
	})


def get_clockedin_emps(request):
    emps = Shift.objects.filter(date=dj_tz.localtime()).values_list('employee', flat=True)
    data = {'active_emps': list(emps)}
    return JsonResponse(data)