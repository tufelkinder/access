$(function() {
	$('#id_include_active').change(function() {
		if ($(this).is(':checked')) {
			let url = '/get_in_emps/';
			fetch(url, {
				credentials: 'include',
				headers: { 'Content-Type': 'application/json' },        
			}).then(response => response.json())
			.then(function(resp) {
				for (const e in resp.active_emps) {
					$(`#id_employees_from option[value='${e}']`).remove().appendTo('#id_employees_to');
				}
			});
		}
	});
});