from django.contrib import admin
from server.models import *

# Register your models here.

class CredentialInline(admin.TabularInline):
    model = Credential
    extra = 1


class HolidayDateInline(admin.TabularInline):
    model = HolidayDate
    extra = 0


class WeekDayAdmin(admin.ModelAdmin):
    list_display = ('name',)


class ServerConfigAdmin(admin.ModelAdmin):
    list_display = ('name',)


class CredentialTypeAdmin(admin.ModelAdmin):
    list_display = ('name',)


class PersonAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'email', 'phone', 'ssn', 'emp_class', 'division')
    list_editable = ('phone', 'ssn', 'emp_class', 'division')
    list_filter = ('emp_class',)
    search_fields = ('phone', 'first_name', 'last_name', 'credential__data')
    filter_horizontal = ('groups', )
    inlines = [CredentialInline]


class PeopleGroupAdmin(admin.ModelAdmin):
    list_display = ('name', 'member_count')


class CredentialAdmin(admin.ModelAdmin):
    list_display = ('person', 'cred_type', 'data', 'description')


class HolidayAdmin(admin.ModelAdmin):
    list_display = ('name', 'date', 'is_recurring')
    inlines = [HolidayDateInline]


class TimeZoneAdmin(admin.ModelAdmin):
    list_display = ('time_start', 'time_end',)
    filter_horizontal = ('days', 'except_holidays')


class ControllerAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'server', 'ip_addr', 'channels')


class DoorGroupAdmin(admin.ModelAdmin):
    list_display = ('name',)


class DoorAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'controller', 'time_clock', 'lock_state')  # 'unlock_button', 'lock_button'
    filter_horizontal = ('groups', )
    list_editable = ('time_clock',)


class TimeClockAdmin(admin.ModelAdmin):
    list_display = ('name',)


class AccessRuleAdmin(admin.ModelAdmin):
    list_display = ('name', 'is_active')


class EventTypeAdmin(admin.ModelAdmin):
    list_display = ('name',)


class EventAdmin(admin.ModelAdmin):
    list_display = ('date_time', 'event_type', 'person', 'cred_data', 'door', 'description')
    list_filter = ('event_type', 'person', 'door')


class DivisionAdmin(admin.ModelAdmin):
    list_display = ('name', 'location',)


class LogAdmin(admin.ModelAdmin):
    list_display = ('date_created', 'message')


admin.site.register(WeekDay, WeekDayAdmin)
admin.site.register(ServerConfig, ServerConfigAdmin)
admin.site.register(CredentialType, CredentialTypeAdmin)
admin.site.register(Person, PersonAdmin)
admin.site.register(PeopleGroup, PeopleGroupAdmin)
admin.site.register(Credential, CredentialAdmin)
admin.site.register(Holiday, HolidayAdmin)
admin.site.register(TimeZone, TimeZoneAdmin)
admin.site.register(Controller, ControllerAdmin)
admin.site.register(DoorGroup, DoorGroupAdmin)
admin.site.register(Door, DoorAdmin)
admin.site.register(TimeClock, TimeClockAdmin)
admin.site.register(AccessRule, AccessRuleAdmin)
admin.site.register(EventType, EventTypeAdmin)
admin.site.register(Event, EventAdmin)
admin.site.register(Division, DivisionAdmin)
admin.site.register(Log, LogAdmin)
