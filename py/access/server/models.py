import json
from datetime import date, datetime
from django.utils.translation import ugettext as _
from django.db import models
from django.contrib.auth.models import User, Group
from django.db.models import Q  #, F, Max, Min, Count, Sum, Avg
from cryptography.hazmat.primitives import serialization as crypto_serialization, hashes
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend as crypto_default_backend
from cryptography.hazmat.primitives.serialization import load_pem_private_key, load_pem_public_key
from server import actions as a
from django.utils import timezone as dj_tz

# Create your models here.


class BaseModel(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL, related_name="%(class)s_created_by", editable=False)
    modified_by = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL, related_name="%(class)s_modified_by", editable=False)

    class Meta:
        abstract = True


class WeekDay(models.Model):
    name = models.CharField(max_length=255, null=True)
    tag = models.CharField(max_length=25, null=True, blank=True)

    def __str__(self):
        return self.name


class ServerConfig(BaseModel):
    name = models.CharField(max_length=255, null=True, blank=True)
    private_key = models.TextField(null=True, blank=True)
    public_key = models.TextField(null=True, blank=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        super(ServerConfig, self).save(*args, **kwargs)
        if not self.private_key:
            self.generate_keys()

    def generate_keys(self):
        key = rsa.generate_private_key(
            backend=crypto_default_backend(),
            public_exponent=65537,
            key_size=2048
        )
        self.private_key = key.private_bytes(
            crypto_serialization.Encoding.PEM,
            crypto_serialization.PrivateFormat.PKCS8,
            crypto_serialization.NoEncryption()).decode('utf-8')
        self.public_key = key.public_key().public_bytes(
            crypto_serialization.Encoding.PEM,
            crypto_serialization.PublicFormat.PKCS1).decode('utf-8')
        self.save()


class CredentialType(models.Model):
    name = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.name


class PeopleGroup(BaseModel):
    name = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.name

    def member_count(self):
        return Person.objects.filter(groups=self).count()


class Division(models.Model):
    name = models.CharField(max_length=100)
    location = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.name

    def short_loc(self):
        return self.location[0:4] if self.location else ''


class Person(BaseModel):
    CLASSES = (('S', 'Salary'), ('F', 'Full-time'), ('P', 'Part-time'))
    user = models.OneToOneField(User, null=True, blank=True, on_delete=models.PROTECT, related_name='employee')
    employee_id = models.CharField(max_length=100, null=True, blank=True)
    phone = models.CharField(max_length=100, null=True, blank=True)
    first_name = models.CharField(max_length=255, null=True)
    last_name = models.CharField(max_length=255, null=True)
    email = models.CharField(max_length=255, null=True, blank=True)
    groups = models.ManyToManyField('PeopleGroup', blank=True)
    division = models.ForeignKey('Division', null=True, blank=True, default=1, on_delete=models.SET_NULL)
    emp_class = models.CharField(max_length=100, null=True, blank=True, choices=CLASSES, default='F')
    ssn = models.CharField(max_length=100, null=True, blank=True)
    mobile_clock = models.BooleanField(default=False)
    active = models.BooleanField(default=True)

    class Meta:
        ordering = ('last_name', 'first_name')

    def __str__(self):
        return "{0}, {1}".format(self.last_name, self.first_name)

    def has_access(self, door):
        now = dj_tz.localtime()
        rules = self.accessrule_set.all() | AccessRule.objects.filter(groups__in=self.groups.all())
        rules = rules.filter(
                    Q(doors=door) | Q(door_groups__in=door.groups.all())
                ).filter(
                    Q(timezones__time_start__lte=now.time()) & Q(timezones__time_end__gte=now.time()) & Q(timezones__days__pk=now.isoweekday())
                )
        try:
            holiday = Holiday.objects.get(Q(date=now.date()) | Q(date__month=now.month,date__day=now.day,is_recurring=True))
            rules = rules.filter(~Q(except_holidays=holiday))
        except Holiday.DoesNotExist:
            holiday = None
        if rules:
            return True
        else:
            return False


class Credential(BaseModel):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    cred_type = models.ForeignKey(CredentialType, null=True, on_delete=models.CASCADE)
    data = models.CharField(max_length=255, null=True, unique=True)
    description = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return '{0} - {1}'.format(self.cred_type, self.data[-4:])


class Holiday(BaseModel):
    name = models.CharField(max_length=255)
    date = models.DateField(null=True, unique=True)
    is_recurring = models.BooleanField(default=True, help_text='Recurs annually on the same date.')

    def __str__(self):
        return self.name

    # def __eq__(self, other): 
    #     # possibly need to test for datetime vs. date object?
    #     return self.date.__dict__ == other.__dict__


class HolidayDate(BaseModel):
    holiday = models.ForeignKey('Holiday', on_delete=models.CASCADE)
    date = models.DateField(null=True, unique=True)
    date_observed = models.DateField(null=True, unique=True)

    def save(self, *args, **kwargs):
        if self.date and not self.date_observed:
            self.date_observed = self.date
        super().save(*args, **kwargs)


class TimeZone(BaseModel):
    time_start = models.TimeField()
    time_end = models.TimeField()
    days = models.ManyToManyField(WeekDay)
    except_holidays = models.ManyToManyField(Holiday, blank=True)

    def __str__(self):
        days = ', '.join(self.days.all().values_list('tag', flat=True))
        return 'From {0} to {1} on {2}'.format(self.time_start, self.time_end, days)


class Controller(BaseModel):
    name = models.CharField(max_length=255)
    server = models.ForeignKey(ServerConfig, null=True, blank=True, on_delete=models.SET_NULL)
    ip_addr = models.CharField(verbose_name='IP Address', max_length=100, null=True, blank=True)
    channels = models.IntegerField(null=True, blank=True, default=1, help_text='Number of doors connected to this controller.')
    public_key = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class DoorGroup(BaseModel):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Door(BaseModel):
    name = models.CharField(max_length=255)
    groups = models.ManyToManyField(DoorGroup, blank=True)
    controller = models.ForeignKey(Controller, null=True, blank=True, on_delete=models.SET_NULL)
    lock_state = models.CharField(max_length=100, null=True, blank=True, editable=False)
    time_clock = models.ForeignKey('server.TimeClock', null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.name

    def unlock(self):  # should these even be here?
        pass

    def unlock_button(self):
        return '<button class="deletelink">Unlock</button>'
    unlock_button.allow_tags = True

    def lock(self):  # should these even be here?
        pass

    def lock_button(self):
        return '<button class="default">Lock</button>'
    lock_button.allow_tags = True


class TimeClock(BaseModel):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class AccessRule(BaseModel):
    name = models.CharField(max_length=255)
    persons = models.ManyToManyField(Person, blank=True)
    groups = models.ManyToManyField(PeopleGroup, blank=True)
    doors = models.ManyToManyField(Door, blank=True)
    door_groups = models.ManyToManyField(DoorGroup, blank=True)
    timezones = models.ManyToManyField('TimeZone', blank=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class EventType(models.Model):
    """ 
        door open, door closed, door open too long,
        credential accepted, credential rejected,
        lock unlocked, lock locked, clock in, clock out
    """
    name = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.name


class Event(models.Model):
    event_type = models.ForeignKey(EventType, null=True, blank=True, on_delete=models.SET_NULL)
    date_time = models.DateTimeField(auto_now_add=True)
    person = models.ForeignKey(Person, null=True, blank=True, on_delete=models.SET_NULL)
    cred_data = models.CharField(max_length=255, null=True, blank=True)
    door = models.ForeignKey(Door, null=True, blank=True, on_delete=models.SET_NULL)
    description = models.CharField(max_length=255, null=True, blank=True)


class Log(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    message = models.TextField(null=True, blank=True)


