import time
import os
from django.db import models
from django.db.models import Q  #, F, Max, Min, Count, Sum, Avg
from django.contrib.auth.models import User, Group
from cryptography.hazmat.primitives import serialization as crypto_serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend as crypto_default_backend
from server import actions as a
from server.models import BaseModel

if os.uname()[4].startswith("arm"):
    import RPi.GPIO as IO
else:
    IO = None

# Create your models here.


class ServerConnection(BaseModel):
    name = models.CharField(max_length=100, null=True)
    server_ip = models.CharField(max_length=100, null=True, blank=True)
    controller_id = models.IntegerField(null=True, help_text="Unique ID for this controller's communication with the server.")
    public_key = models.TextField(null=True, blank=True)
    private_key = models.TextField(null=True, blank=True)
    server_pub_key = models.TextField(null=True, blank=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        super(ServerConnection, self).save(*args, **kwargs)
        if not self.private_key:
            self.generate_keys()

    def generate_keys(self):
        key = rsa.generate_private_key(
            backend=crypto_default_backend(),
            public_exponent=65537,
            key_size=2048
        )
        self.private_key = key.private_bytes(
            crypto_serialization.Encoding.PEM,
            crypto_serialization.PrivateFormat.PKCS8,
            crypto_serialization.NoEncryption()).decode('utf-8')
        self.public_key = key.public_key().public_bytes(
            crypto_serialization.Encoding.PEM,
            crypto_serialization.PublicFormat.PKCS1).decode('utf-8')
        self.save()


class RawConnectionType(models.Model):
    """ sensor / relay """
    name = models.CharField(max_length=100, null=True, blank=True, choices=(('relay', 'Relay'), ('sensor', 'Sensor')))

    def __str__(self):
        return self.name


class ConnectionType(models.Model):
    name = models.CharField(max_length=255)
    raw_conn_type = models.ForeignKey(RawConnectionType, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.name


class DoorConfig(BaseModel):
    name = models.CharField(max_length=255, null=True)
    door_id = models.IntegerField(null=True, blank=True, unique=True, help_text='Server door ID.')
    open_delay = models.IntegerField(null=True, default=12, help_text='In whole seconds.')
    lock_state = models.CharField(max_length=100, null=True, blank=True, choices=(('unlocked', 'Unlocked'),('locked', 'Locked')), editable=False)  # noqa

    def __str__(self):
        return self.name

    def unlock(self):
        lock_conn = self.connection_set.get(conn_type__pk=1)
        lock_conn.trigger(a.UNLOCK)
        self.lock_state = 'unlocked'
        self.save()
        # send server event: door, state

    def lock(self):
        lock_conn = self.connection_set.get(conn_type__pk=1)
        lock_conn.trigger(a.LOCK)
        self.lock_state = 'locked'
        self.save()
        # send server event: door, state

    def momentary(self):
        # send server event: door, state
        self.unlock()
        time.sleep(self.open_delay)
        self.lock()


class Connection(BaseModel):
    door_config = models.ForeignKey(DoorConfig, null=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=255, null=True)
    conn_type = models.ForeignKey(ConnectionType, null=True, on_delete=models.SET_NULL)
    pos_data0 = models.IntegerField(null=True, help_text='Positive or data 0 pin.')
    neg_data1 = models.IntegerField(null=True, blank=True, help_text='Negative or data 1 pin.')
    lock_state = models.CharField(max_length=100, null=True, blank=True, choices=(('open', 'Open'),('closed', 'Closed')))
    normal_state = models.CharField(max_length=100, null=True, blank=True, choices=(('open', 'Open'),('closed', 'Closed')))

    def __str__(self):
        return self.name

    def on(self):
#        IO.setup(self.pos_data0, IO.OUT, IO.LOW)  # assume already setup
        IO.output(self.pos_data0, IO.HIGH)

    def off(self):
#        IO.setup(self.pos_data0, IO.OUT, IO.LOW)  # assume already setup
        IO.output(self.pos_data0, IO.LOW)

    def trigger(self, action):
        red = self.door_config.connection_set.get(conn_type__pk=6)
        green = self.door_config.connection_set.get(conn_type__pk=5)
        door_lock = self  #.door_config.connection_set.get(conn_type__pk=1)

        if action == a.UNLOCK:
            self.off()
            # trigger LED green on, LED red off
            green.on()
            red.off()
        elif action == a.LOCK:
            self.on()
            # trigger LED green off, LED red on
            red.on()
            green.off()


# class TimeClockConfig(BaseModel):
#     name = models.CharField(max_length=255, null=True)
#     clock_id = models.IntegerField(null=True, blank=True, unique=True, help_text='Server timeclock ID.')
#     read_delay = models.IntegerField(null=True, default=2, help_text='Delay before reader will accept another read.')
#     conn_type = models.ForeignKey(ConnectionType, null=True, on_delete=models.SET_NULL)
#     pos_data0 = models.IntegerField(null=True, help_text='Positive or data 0 pin.')
#     neg_data1 = models.IntegerField(null=True, blank=True, help_text='Negative or data 1 pin.')

#     def __str__(self):
#         return self.name


class Log(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    message = models.TextField(null=True, blank=True)
