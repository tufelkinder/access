from django.contrib import admin
from .models import *

# Register your models here.

class ConnectionInline(admin.TabularInline):
    model = Connection
    extra = 1


class RawConnectionTypeAdmin(admin.ModelAdmin):
    list_display = ('name',)


class ConnectionTypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'raw_conn_type')


class ConnectionAdmin(admin.ModelAdmin):
    list_display = ('name', 'conn_type', 'pos_data0', 'neg_data1', 'lock_state', 'normal_state')
    list_filter = ('conn_type',)


class DoorConfigAdmin(admin.ModelAdmin):
    list_display = ('name', 'door_id', 'open_delay', 'lock_state')
    inlines = [ConnectionInline]


class ServerConnectionAdmin(admin.ModelAdmin):
    list_display = ('name', 'server_ip', 'controller_id', 'is_active')


class LogAdmin(admin.ModelAdmin):
    list_display = ('date_created', 'message')


admin.site.register(ServerConnection, ServerConnectionAdmin)
admin.site.register(RawConnectionType, RawConnectionTypeAdmin)
admin.site.register(ConnectionType, ConnectionTypeAdmin)
admin.site.register(Connection, ConnectionAdmin)
admin.site.register(DoorConfig, DoorConfigAdmin)
admin.site.register(Log, LogAdmin)
