"""
Reads from HID 6005 reader
"""

import RPi.GPIO as IO 
import time

D0 = 12
D1 = 25

D_PINS = {D0:0, D1:1}

IO.setmode(IO.BCM)

IO.setup(D0, IO.IN)
IO.setup(D1, IO.IN)

result = []

def my_callback(p):
    result.append(D_PINS[p])

IO.add_event_detect(D0, IO.RISING, callback=my_callback)
IO.add_event_detect(D1, IO.RISING, callback=my_callback)

while True:
    if len(result) > 0:
        out = 0
        for bit in result:
            out = (out << 1) | bit
        print(result, out)
        result = []
    else:
        time.sleep(.25)

IO.cleanup()

