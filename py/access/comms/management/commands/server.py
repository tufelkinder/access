import daemon
import socketserver
from pytz import timezone
from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone as dj_tz
from comms.signals import *
from access.settings import COMM_IP, SRV_PORT
from server import actions as a
from server.models import *
from timeclock.models import ClockEvent

EST = timezone('US/Eastern')


class TCPHandler(socketserver.BaseRequestHandler):

    def handle(self):
        # self.request is the TCP socket connected to the client
        # self.client_address[0] # if needed later
        # data must contain:
        #    - controller_id
        #    - door_id
        #    - action
        # data may contain:
        #    - credential
        fatal = False
        self.data = self.request.recv(1024)  # do I need this? .strip()
        log = Log.objects.create(message='Signal received: {}'.format(self.data))
        # decrypt data
        try:
            sc = ServerConfig.objects.get(pk=1)  # will fail without correct ServerConfig!
            data = decrypt_signal(self.data, sc.private_key)
        except Exception as e:
            print(e, ':', self.data)
            msg = 'Error finding ServerConfig.'
            fatal = True

        try:
            cont = Controller.objects.get(pk=data['controller_id'])
            ctrl_key = cont.public_key
        except Exception as e:
            print(e)
            msg = 'Controller not found: {0}'.format(data['controller_id'])
            fatal = True

        try:
            door = Door.objects.get(pk=data['door_id'])
            evt = Event.objects.create(
                event_type_id=a.READ,
                cred_data=data['credential'],
                door=door,
            )
            if door.time_clock:
                evt = Event.objects.create(
                    event_type_id=a.CLOCK,
                    cred_data=data['credential'],
                    door=door,
                )
                data['action'] = a.CLOCK
        except Exception as e:
            print(e)
            msg = 'Door not found: {0}'.format(data['door_id'])
            evt = Event.objects.create(
                event_type_id=a.NO_SUCH_DOOR,
                cred_data=data['credential'],
                description='Access attempted for non-existent door: {}'.format(data['door_id'])
            )
            fatal = True

        if not fatal:
            if data['action'] == a.READ:
                # try to find credentials
                try:
                    cred = Credential.objects.get(data=data['credential'])
                except:
                    evt = Event.objects.create(
                        event_type_id=a.NO_SUCH_CRED,
                        cred_data=data['credential'],
                        description='Access attempted for UNKNOWN credential: {}'.format(data['credential'])
                    )
                # check user perms
                if cred.person.has_access(door):
                # log GRANTED or DENIED
                    data = { 'action': a.GRANTED, 'door_id': door.id }
                    evt = Event.objects.create(
                        event_type_id=a.GRANTED,
                        person=cred.person,
                        cred_data=cred.data,
                        description='Access GRANTED for {0} credential {1}'.format(cred.person, cred)
                    )
                else:
                    data = { 'action': a.DENIED, 'door_id': door.id }
                    evt = Event.objects.create(
                        event_type_id=a.DENIED,
                        person=cred.person,
                        cred_data=cred.data,
                        description='Access DENIED for {0} credential {1}'.format(cred.person, cred)
                    )
                # create MOMENTARY action data
                # encrypt and transmit back with reply
                self.request.sendall(encrypt_signal(data, ctrl_key))
            elif data['action'] == a.CLOCK:
                try:
                    cred = Credential.objects.get(data=data['credential'])
                    ce = ClockEvent(
                        time_clock=door.time_clock,
                        employee=cred.person,
                        date_time=dj_tz.localtime()
                    )
                    ce.save()
                    data = { 'action': a.GRANTED, 'door_id': door.id }
                except Credential.DoesNotExist:
                    data = { 'action': a.DENIED, 'door_id': door.id }
                    evt = Event.objects.create(
                        event_type_id=a.NO_SUCH_CRED,
                        cred_data=data['credential'],
                        description='Clock read attempted for UNKNOWN credential: {}'.format(data['credential'])
                    )
                self.request.sendall(encrypt_signal(data, ctrl_key))
            else:
                # log signal as event
                evt = Event.objects.create(
                    event_type_id=data['action'],
                    door=door,
                    description='Event logged for controller {0}.'.format(data['controller_id'])
                )
                # acknowledge
                data = {'action': a.ACK}
                self.request.sendall(encrypt_signal(data, ctrl_key))  # granted or denied...
        else:
            if ctrl_key:
                data = {'error': msg}
                self.request.sendall(encrypt_signal(data, ctrl_key))  # granted or denied...
            else:
                pass
            print(msg)


def execute_main():
    HOST, PORT = COMM_IP, 9856

    # Create the server, binding to localhost on port 9856
    server = socketserver.TCPServer((HOST, PORT), TCPHandler)

    # Activate the server; this will keep running until you
    # interrupt the program with Ctrl-C
    server.serve_forever()


class Command(BaseCommand):
    help = 'Starts server daemon'

    def handle(self, *args, **options):
        # with daemon.DaemonContext():
        execute_main()


if __name__ == "__main__":
    execute_main()