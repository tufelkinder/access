import logging
logging.basicConfig(filename='/var/log/access-controller_err.log',level=logging.DEBUG)
import traceback
import daemon
import time
import RPi.GPIO as IO
from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q  #, F, Max, Min, Count, Sum, Avg
from comms.signals import *
from server import actions as a
from controller.models import *
from server.models import Event


D_PINS = {}
result = []


def handle_read(io_pin):
    global result
    global D_PINS
    result.append(D_PINS[io_pin])


def handle_rqe(io_pin):
    # momentary unlock door
    conn = Connection.objects.get(Q(pos_data0=io_pin) | Q(neg_data1=io_pin))
    conn.door_config.momentary()


def execute_main():
    global result
    global D_PINS
    count = 0
    sc = ServerConnection.objects.filter(is_active=True)[0]
    IO.cleanup()
    IO.setmode(IO.BCM)
    # should only be one active DoorConfig
    for dc in DoorConfig.objects.all():
        for conn in dc.connection_set.all():
            if conn.conn_type.pk == 1:  # door lock
                pin = conn.pos_data0
                IO.setup(pin, IO.OUT)
                IO.output(pin, IO.HIGH)  # should change depending on connection lock / normal settings??
            elif conn.conn_type.pk == 2:  # door open sensor
                # log when it opens
                # log when it closes
                # send events to server...??
                pass
            elif conn.conn_type.pk == 3:  # rqe sensor
                pin = conn.pos_data0
                IO.setup(pin, IO.IN)
                IO.add_event_detect(pin, IO.FALLING, callback=handle_rqe)
            elif conn.conn_type.pk == 4:  # reader
                d0, d1 = conn.pos_data0, conn.neg_data1
                D_PINS[d0], D_PINS[d1] = 0, 1
                IO.setup(d0, IO.IN, pull_up_down=IO.PUD_DOWN)
                IO.setup(d1, IO.IN, pull_up_down=IO.PUD_DOWN)
                IO.add_event_detect(d0, IO.RISING, callback=handle_read)
                IO.add_event_detect(d1, IO.RISING, callback=handle_read)
            elif conn.conn_type.pk == 5:  # green led
                pin = conn.pos_data0
                IO.setup(pin, IO.OUT)
                IO.output(pin, IO.LOW)  # should change depending on connection lock / normal settings??
            elif conn.conn_type.pk == 6:  # red led
                pin = conn.pos_data0
                IO.setup(pin, IO.OUT)
                IO.output(pin, IO.HIGH)  # should change depending on connection lock / normal settings??
    while True:
        if len(result) > 0:
            cred = 0
            for bit in result[1:-1]:  # stripping parity bits... ignore and just convert with them??
                cred = (cred << 1) | bit
            # check permissions on this card number...
            data = {
                'action': a.READ,
                'door_id': dc.door_id,
                'credential': cred,
                'controller_id': sc.controller_id
            }
            print(data)
            log = Log.objects.create(message=str(data))
                # send request to server
            try:
                msg = send_to_server(data, 1)
                resp = decrypt_signal(msg, sc.private_key)
                log = Log.objects.create(message=str(resp))
                # open door if granted
                if resp['action'] == a.GRANTED:
                    dc.momentary()
            except Exception as e:
                logging.exception("error: controller io, line 95")
                tb = traceback.format_exc()
                log = Log.objects.create(message='controller error: {0}\n{1}'.format(e, tb))
            result = []
            count = 0
        elif count > 4:
            result = []
            count = 0
        else:
            time.sleep(.5)
            count += 1


if __name__ == "__main__":

    with daemon.DaemonContext():
        execute_main()    


class Command(BaseCommand):
    help = 'Starts controller daemon.'

    def handle(self, *args, **options):
        # with daemon.DaemonContext():
        execute_main()

