import socket
import json
import base64
from controller import models as c_models
from server import models as s_models
from access.settings import COMM_IP, CTRL_PORT, SRV_PORT
from cryptography.hazmat.primitives import serialization as crypto_serialization, hashes
from cryptography.hazmat.primitives.asymmetric import rsa, padding
from cryptography.hazmat.backends import default_backend as crypto_default_backend
from cryptography.hazmat.primitives.serialization import load_pem_private_key, load_pem_public_key


def test_decrypt_signal(msg, key):
    return json.loads(msg.decode('utf-8'))


def test_encrypt_signal(data, key):
    return json.dumps(data).encode('utf-8')


def decrypt_signal(msg, key):
    """
        JSON payload:
        controller id
        event type id
        door id
        action type:    add event (if update door lock state, update door lock state as well)
                        credential scanned/entered
    """
    private_key = load_pem_private_key(key.encode('utf-8'), password=None, backend=crypto_default_backend())
    message = private_key.decrypt(
        base64.b64decode(msg),
        padding.OAEP(mgf=padding.MGF1(algorithm=hashes.SHA1()), algorithm=hashes.SHA1(), label=None)
    )
    data = json.loads(message.decode('utf-8'))
    return data


def encrypt_signal(data, key):
    """
        data: should be a dictionary containing:
            door: id
            action type: a.UNLOCK, a.LOCK, a.MOMENTARY
        controller_id: destination controller
    """
    msg = json.dumps(data)
    public_key = load_pem_public_key(key.encode('utf-8'), backend=crypto_default_backend())
    ciphertext = public_key.encrypt(
        msg.encode('utf-8'),
        padding.OAEP(mgf=padding.MGF1(algorithm=hashes.SHA1()), algorithm=hashes.SHA1(), label=None)
    )
    return base64.b64encode(ciphertext)


def send_to_controller(data, controller_id):
    ctrl = s_models.Controller.objects.get(pk=controller_id)
    msg = encrypt_signal(data, ctrl.public_key)
    received = None

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        sock.connect((COMM_IP, CTRL_PORT))
        sock.sendall(msg)
        received = sock.recv(1024)
    finally:
        sock.close()

    return received


def send_to_server(data, sc_id):
    """ data = unencrypted dict """
    sc = c_models.ServerConnection.objects.get(pk=sc_id)
    msg = encrypt_signal(data, sc.server_pub_key)
    received = None

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        sock.connect((sc.server_ip, SRV_PORT))
        sock.sendall(msg)
        received = sock.recv(1024)
    finally:
        sock.close()
    return received
