import socketserver
from .signals import *
from access.settings import COMM_IP
from controller.models import *
from server import actions as a


class TCPHandler(socketserver.BaseRequestHandler):

    def handle(self):
        # self.request is the TCP socket connected to the client
        self.data = self.request.recv(1024).strip()
        print("{} wrote:".format(self.client_address[0]))
        print(self.data)
        # just send back the same data, but upper-cased
        self.request.sendall() 


def execute_main():
    HOST, PORT = COMM_IP, 9858

    # Create the server, binding to localhost on port 9856
    server = socketserver.TCPServer((HOST, PORT), TCPHandler)

    # Activate the server; this will keep running until you
    # interrupt the program with Ctrl-C
    server.serve_forever()


if __name__ == "__main__":
    execute_main()

