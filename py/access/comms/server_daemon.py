import socketserver
from .signals import *
from access.settings import COMM_IP
from server.models import *
from server import actions as a


class TCPHandler(socketserver.BaseRequestHandler):

    def handle(self):
        # self.request is the TCP socket connected to the client
        # self.client_address[0] # if needed later
        try:
            sc = ServerConfig.objects.get(pk=1)  # will fail without correct ServerConfig!
        except:
            msg = 'Error finding ServerConfig.'
            fatal = True
        try:
            cont = Controller.objects.get(pk=data['controller_id'])
            key = cont.public_key
        except:
            msg = 'Controller not found: {0}'.format(data['controller_id'])
            fatal = True

        if not fatal:
            self.data = self.request.recv(1024).strip()
            print(self.data)
            # decrypt data
            data = decrypt_signal(self.data, sc.private_key)
            if data['action'] == a.READ:
                try:
                    door = Door.objects.get(pk=data['door_id'])
                    evt = Event.objects.create(
                        event_type_id=a.READ,
                        cred_data=data['credential'],
                        door=door,
                    )
                except:
                    evt = Event.objects.create(
                        event_type_id=a.NO_SUCH_DOOR,
                        cred_data=data['credential'],
                        description='Access attempted for non-existent door: {}'.format(data['door_id'])
                    )

                # try to find credentials
                try:
                    cred = Credential.objects.get(data=data['credential'])
                except:
                    evt = Event.objects.create(
                        event_type_id=a.NO_SUCH_CRED,
                        cred_data=data['credential'],
                        description='Access attempted for non-existent credential: {}'.format(data['credential'])
                    )
                # check user perms
                if cred.person.has_access(door):
                # log GRANTED or DENIED
                    data = { 'action': a.MOMENTARY, 'door_id': door.id }
                    evt = Event.objects.create(
                        event_type_id=a.GRANTED,
                        person=cred.person,
                        cred_data=data['credential'],
                        description='Access GRANTED for {0} credential {1}'.format(cred.person, cred)
                    )
                else:
                    data = { 'action': a.DENIED, 'door_id': door.id }
                    evt = Event.objects.create(
                        event_type_id=a.DENIED,
                        person=cred.person,
                        cred_data=data['credential'],
                        description='Access DENIED for {0} credential {1}'.format(cred.person, cred)
                    )
                # create MOMENTARY action data
                # encrypt and transmit back with reply
                self.request.sendall(encrypt_signal(data, ctrl_key))
            else:
                data = {'action': a.ACK}
                self.request.sendall(encrypt_signal(data, ctrl_key))  # granted or denied...
        else:
            if ctrl_key:
                data = {'error': msg}
                self.request.sendall(encrypt_signal(data, ctrl_key))  # granted or denied...
            else:
                pass
            print(msg)


def execute_main():
    HOST, PORT = COMM_IP, 9856

    # Create the server, binding to localhost on port 9856
    server = socketserver.TCPServer((HOST, PORT), TCPHandler)

    # Activate the server; this will keep running until you
    # interrupt the program with Ctrl-C
    server.serve_forever()


if __name__ == "__main__":
    execute_main()
