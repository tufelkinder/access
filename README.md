# django-access #

Python/Django-based Access Control software, designed to run as a client-server suite on a Raspberry Pi as a door controller with a central system as a management server.

### Installation ###

* Coming soon
* [One of these days](https://bitbucket.org/tutorials/markdowndemo)

### Server ###

* python manage.py server

### Controller ###

* python manage.py controller

### Needs ###

* Reader Interface (in progress)
* Tests